package br.edu.cest.prova;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	Livro livro1 = new Livro();
    	livro1.setAutor("Matheuszada");
    	livro1.setEdicao("3º");
    	livro1.setVolume("4");
    	
    	Item item1 = new Item();
    	item1.setTitulo("Scroll");
    	item1.setAnoPublicacao("2000");
    	item1.setEditora("blabla");
    	item1.setIsbn("654218135846");
    	item1.setPreco("10$");
    	
    	livro1.display();
    	item1.display();
    	
    	ParImpar numero = new ParImpar();
    	numero.valor();
    }
}
