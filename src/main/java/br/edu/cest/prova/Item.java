package br.edu.cest.prova;

public class Item {
	private String titulo;
	private String editora;
	private String anoPublicacao;
	private String isbn;
	private String preco;
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getEditora() {
		return editora;
	}
	public void setEditora(String editora) {
		this.editora = editora;
	}
	public String getAnoPublicacao() {
		return anoPublicacao;
	}
	public void setAnoPublicacao(String anoPublicacao) {
		this.anoPublicacao = anoPublicacao;
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public String getPreco() {
		return preco;
	}
	public void setPreco(String preco) {
		this.preco = preco;
	}
	
	public void display() {
		System.out.println("titulo:"+this.getTitulo());
		System.out.println("preco:"+this.getPreco());
		System.out.println("editora:"+this.getEditora());
		System.out.println("ano de publicação:"+this.getAnoPublicacao());
		System.out.println("isbn:"+this.getIsbn());
	}

}
