package br.edu.cest.prova;

public class Livro extends Item {
	private String autor;
	private String edicao;
	private String volume;
	public String getAutor() {
		return autor;
	}
	public void setAutor(String autor) {
		this.autor = autor;
	}
	public String getEdicao() {
		return edicao;
	}
	public void setEdicao(String edicao) {
		this.edicao = edicao;
	}
	public String getVolume() {
		return volume;
	}
	public void setVolume(String volume) {
		this.volume = volume;
	}
	
	@Override
	public void display() {
		System.out.println("autor:"+this.getAutor());
		System.out.println("edição:"+this.getEdicao());
		System.out.println("volume:"+this.getVolume());
	}
			
	
	

}
